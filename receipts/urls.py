from django.urls import path
from receipts.views import (
    show_receipt_list,
    create_receipt,
    show_category_list,
    show_account_list,
)

urlpatterns = [
    path("", show_receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_category_list, name="catagory_list"),
    path("accounts/", show_account_list, name="account_list"),
]
